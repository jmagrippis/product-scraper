# Sainsbury's Product Scraper

A console application that scrapes Sainsbury's grocery site and returns a JSON array of the listed products.

## Installation

You will need [git][] and [composer][] to install this application.

- Clone the git repository, for example ```git clone git@bitbucket.org:jmagrippis/product-scraper.git``` via the CLI
- cd into the project's root directory
- ```composer install -o```

## Usage

In the project root, run:

```./bin/scraper.php parse:uri```

For simplicity's sake, the command above assumes the assignment uri as its target and prints out a json object.

As this was built as if it were an actual assignment and not a test, you can actually pass any uri as a parameter 
and specify the format using the ```--format``` flag.

Because it *is* a test though, it will likely require tweaking to produce meaningful results 
when given uris other than the test page, and it does not offer any other formatting/output options.

You can, however, pass the ```--pretty``` flag for json-prettified output 
and ```-v``` or ```--verbose``` for the "play-by-play" process.

## Testing

This application employs [BDD][].

[phpspec][] is the test runner of choice at the spec level. To run all specs, simply execute
```./vendor/bin/phpspec run``` at the root of the project.
 
At the story level, we have [Codeception][]. There is not much story to this project, 
but you can test the single scenario we have defined by executing ```./vendor/bin/codecept run functional```
at the root of the project.
 
## Notice

This application was created as a technical test by [Johnny Magrippis][].

It is available in a public repository for review, but does maintain a proprietary license.

[git]: https://git-scm.com/ "version control system of choice"
[composer]: https://getcomposer.org/ "THE php package manager"

[BDD]: https://en.wikipedia.org/wiki/Behavior-driven_development "Behaviour-driven development"
[phpspec]: http://phpspec.readthedocs.org/en/latest/ "Unit tester of choice"
[Codeception]: http://codeception.com/ "Functional/Acceptance tester of choice"

[Johnny Magrippis]: http://magrippis.com "Johnny Magrippis' showcase/portfolio site"