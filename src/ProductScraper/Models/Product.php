<?php

namespace ProductScraper\Models;


/**
 * Class Product
 *
 * @package ProductScraper\Models
 */
class Product implements \JsonSerializable
{
    /**
     * @var array
     */
    protected $properties;

    /**
     * The properties we allow to be mass-assigned.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'size',
        'price',
        'description',
        'direct_uri'
    ];

    /**
     * Product constructor.
     *
     * @param array $properties
     */
    public function __construct(array $properties = [])
    {
        $this->properties = [];
        foreach ($this->filterFillableProperties($properties) as $property => $value) {
            $this->properties[$property] = $value;
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $size = $this->getSize();
        return [
            'title' => $this->getTitle(),
            'size' => $size ? $size / 1000 . 'kb' : null,
            'unit_price' => $this->getPrice(),
            'description' => $this->getDescription()
        ];
    }

    /**
     * Magic getter!
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property)
    {
        $method = 'get' . ucfirst($property);
        if (!method_exists($this, $method)) {
            return false;
        }
        return $this->$method();
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Returns the title of the product.
     *
     * @return string|null
     */
    public function getTitle()
    {
        $properties = $this->getProperties();
        return isset($properties['title']) ? $properties['title'] : null;
    }

    /**
     * Returns the size of the product page, in kb.
     *
     * @return float|null
     */
    public function getSize()
    {
        $properties = $this->getProperties();
        return isset($properties['size']) ? $properties['size'] : null;
    }

    /**
     * Returns the price of the product.
     *
     * @return float|null
     */
    public function getPrice()
    {
        $properties = $this->getProperties();
        return isset($properties['price']) ? $properties['price'] : null;
    }

    /**
     * Returns the description of the product.
     *
     * @return string|null
     */
    public function getDescription()
    {
        $properties = $this->getProperties();
        return isset($properties['description']) ? $properties['description'] : null;
    }

    /**
     * Returns the direct uri of the product.
     *
     * @return string|null
     */
    public function getDirectUri()
    {
        $properties = $this->getProperties();
        return isset($properties['direct_uri']) ? $properties['direct_uri'] : null;
    }

    /**
     * Magic setter!
     *
     * @param string $property
     * @param mixed $value
     * @return bool
     */
    public function __set($property, $value)
    {
        $method = 'set' . ucfirst($property);
        if (method_exists($this, $method)) {
            return $this->$method($value);
        }
        return $this->setProperty($property, $value);
    }

    /**
     * Sets the given property to the given values.
     *
     * @param $property
     * @param $value
     * @return bool
     */
    public function setProperty($property, $value)
    {
        $this->properties[$property] = $value;
        return true;
    }

    /**
     * Adds new properties, without overwriting those already set.
     *
     * @param array $added_properties
     * @return Product
     */
    public function withAddedProperties(array $added_properties)
    {
        $expanded_product = clone $this;

        foreach ($this->filterFillableProperties($added_properties) as $property => $value) {
            if ($expanded_product->$property) {
                continue;
            }
            $expanded_product->$property = $value;
        }

        return $expanded_product;
    }

    /**
     * Filters the given array to return only the properties we allow to be filled.
     *
     * @param $properties
     * @return array
     */
    protected function filterFillableProperties($properties)
    {
        return array_intersect_key($properties, array_flip($this->getFillable()));
    }

    /**
     * @return array
     */
    protected function getFillable()
    {
        return $this->fillable;
    }
}