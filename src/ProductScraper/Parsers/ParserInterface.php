<?php
namespace ProductScraper\Parsers;


use ProductScraper\Models\Product;

/**
 * Interface ParserInterface
 *
 * @package ProductScraper\Parsers
 */
interface ParserInterface
{
    /**
     * Replaces the given products
     *
     * @param string $source
     * @return ParserInterface
     */
    public function withSource($source);

    /**
     * Extracts the products from the existing source.
     *
     * @return Product[]
     */
    public function getProducts();

    /**
     * Extracts the single product from the existing source.
     *
     * @return Product
     */
    public function getProduct();
}