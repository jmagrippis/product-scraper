<?php

namespace ProductScraper\Parsers;


use ProductScraper\Models\Product;

/**
 * Class Html
 *
 * @package ProductScraper\Parsers
 */
class Html implements ParserInterface
{
    /**
     * @var null|string
     */
    protected $source;

    /**
     * @var Product[]
     */
    protected $products;

    /**
     * Html constructor.
     *
     * @param string|null $source
     */
    public function __construct($source = null)
    {
        $this->source = $source;
    }

    /**
     * Replaces the given source, resetting any parsed results.
     *
     * @param string $source
     * @return Html
     */
    public function withSource($source)
    {
        return new Html($source);
    }

    /**
     * @return null|string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Extracts the products from the existing source,
     * if they were not extracted already, and returns them.
     *
     * @return Product[]
     */
    public function getProducts()
    {
        if (!$this->products) {
            $productList = $this->extractBetween($this->getSource(), '<ul class="productLister', '</ul>');

            $htmlProducts = explode('</li>', $productList);
            array_pop($htmlProducts);

            $this->products = [];

            foreach ($htmlProducts as $htmlProduct) {
                $this->products[] = new Product($this->extractProductProperties($htmlProduct));
            }
        }

        return $this->products;
    }

    /**
     * Extracts the single product from the existing source,
     * with only the information derived from the single product page.
     *
     * @return Product
     */
    public function getProduct()
    {
        return new Product([
            'description' => $this->extractProductDescription($this->getSource()),
            'size' => strlen($this->getSource())
        ]);
    }

    /**
     * Extracts anything in the given html between the starting and ending delimiters
     *
     * @param string $html
     * @param string $startString
     * @param string $endString
     * @return string
     */
    protected function extractBetween($html, $startString, $endString)
    {
        $start = mb_strpos($html, $startString) + strlen($startString);
        $end = mb_strpos(mb_substr($html, $start), $endString);
        $directUri = mb_substr($html, $start, $end);
        return $directUri;
    }

    /**
     * @param string $html
     * @return array
     */
    protected function extractProductProperties($html)
    {
        $uri = $this->extractProductUri($html);

        return [
            'title' => $this->extractProductTitle($html, $uri),
            'price' => $this->extractProductUnitPrice($html),
            'direct_uri' => $uri
        ];
    }

    /**
     * Helper class to extracts the first uri found in the given html.
     *
     * @param string $html
     * @return string
     */
    protected function extractProductUri($html)
    {
        return $this->extractBetween($html, '<a href="', '"');
    }

    /**
     * Helper class to extract the unit price of the product.
     *
     * @param string $html
     * @return float
     */
    protected function extractProductUnitPrice($html)
    {
        $unitPriceHtml = $this->extractBetween($html, '<p class="pricePerUnit">', '</p>');
        return (float)$this->extractBetween($unitPriceHtml, '&pound', '<');
    }

    /**
     * Extracts the product's title from the given html.
     *
     * @param string $html
     * @param string $uri
     * @return string|null
     */
    protected function extractProductTitle($html, $uri)
    {
        $regex = '/<a href="' . str_replace('/', '\/', $uri) . '" >[ \t\n\r]+(.+)/';
        preg_match($regex, $html, $matches);

        return isset($matches[1]) ? rtrim($matches[1]) : null;
    }

    /**
     * Extracts the product's description from the given html.
     * Allows for the possibility of multiple paragraphs, but strips the html tags.
     *
     * @param string $html
     * @return string|null
     */
    protected function extractProductDescription($html)
    {
        $regex = '/<h3 class="productDataItemHeader">Description<\/h3>\n<div class="productText">\n([\S\s]+?)<\/div>/';
        preg_match($regex, $html, $matches);

        if (!isset($matches[1])) {
            return null;
        }

        return rtrim(strip_tags($matches[1]));
    }
}
