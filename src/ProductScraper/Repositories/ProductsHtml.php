<?php

namespace ProductScraper\Repositories;


use GuzzleHttp\ClientInterface;
use GuzzleHttp\Promise\PromiseInterface;
use ProductScraper\Exceptions\HttpResponse as HttpResponseException;
use ProductScraper\Models\Product;
use ProductScraper\Parsers\ParserInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ProductsHtml
 * Derives products by parsing html.
 *
 * @package ProductScraper\Repositories
 */
class ProductsHtml implements ProductsRepositoryInterface
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var ParserInterface
     */
    protected $parser;

    /**
     * ProductsHtml constructor.
     * @param ClientInterface $client
     * @param ParserInterface $parser
     */
    public function __construct(ClientInterface $client, ParserInterface $parser)
    {
        $this->client = $client;
        $this->parser = $parser;
    }

    /**
     * Gets all products in the given uri.
     *
     * @throws HttpResponseException
     *
     * @param $uri
     * @return Product[]
     */
    public function getAllInUri($uri)
    {
        $response = $this->getClient()->request('GET', $uri);

        if ($response->getStatusCode() != 200) {
            throw new HttpResponseException($response->getReasonPhrase(), $response->getStatusCode());
        }

        $products = $this->getParser()->withSource($response->getBody())->getProducts();

        return $products;
    }

    /**
     * Requests all information for the given products, and returns the promises.
     *
     * @param Product[] $products
     * @return PromiseInterface[]
     */
    public function requestAllInfo($products)
    {
        $promises = [];

        foreach ($products as $key => $product) {
            $promise = $this->getClient()->requestAsync('GET', $product->getDirectUri());
            $promises[$key] = $promise;
        }

        return $promises;
    }

    /**
     * Expands the properties of the given product by parsing the product properties from the given response.
     *
     * @throws HttpResponseException
     *
     * @param Product $product
     * @param ResponseInterface $response
     * @return Product
     */
    public function expand($product, $response)
    {
        if ($response->getStatusCode() != 200) {
            throw new HttpResponseException($response->getReasonPhrase(), $response->getStatusCode());
        }

        $expandedProduct = $this->getParser()->withSource($response->getBody())->getProduct();
        return $product->withAddedProperties($expandedProduct->getProperties());
    }

    /**
     * @return ClientInterface
     */
    protected function getClient()
    {
        return $this->client;
    }

    /**
     * @return ParserInterface
     */
    protected function getParser()
    {
        return $this->parser;
    }
}
