<?php

namespace ProductScraper\Repositories;


use GuzzleHttp\Promise\Promise;
use ProductScraper\Models\Product;
use Psr\Http\Message\ResponseInterface;

interface ProductsRepositoryInterface
{
    /**
     * Gets all products in the given uri.
     *
     * @param $uri
     * @return Product[]
     */
    public function getAllInUri($uri);

    /**
     * Requests all information for the given products, and returns the promises.
     *
     * @param Product[] $products
     * @return Promise[]
     */
    public function requestAllInfo($products);

    /**
     * Expands the properties of the given product by parsing the product properties from the given response.
     *
     * @param Product $product
     * @param ResponseInterface $response
     * @return Product
     */
    public function expand($product, $response);
}