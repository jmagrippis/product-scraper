<?php

namespace ProductScraper\Console\Commands;


use GuzzleHttp\Promise;
use ProductScraper\Models\Product;
use ProductScraper\Repositories\ProductsRepositoryInterface as ProductsRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ParseUri
 * Scrapes a uri and prints out the products found, in the specified format.
 *
 * @package ProductScraper\Console\Commands
 */
class ParseUri extends Command
{
    /**
     * @var ProductsRepository
     */
    protected $productsRepository;

    /**
     * ParseUri constructor.
     *
     * @param $productsRepository
     */
    public function __construct(ProductsRepository $productsRepository)
    {
        $this->productsRepository = $productsRepository;
        parent::__construct('parse:uri');
    }

    /**
     * Sets up the command's parameters, options, name and associated help information.
     *
     */
    protected function configure()
    {
        $this
            ->setDescription('Scrape all products found when GET-requesting a uri')
            ->addArgument(
                'uri',
                InputArgument::OPTIONAL,
                'The specific uri you want to scrape',
                'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html'
            )
            ->addOption(
                'format',
                null,
                InputOption::VALUE_OPTIONAL,
                'The format the scraped products will be returned in',
                'json'
            )
            ->addOption(
                'pretty',
                null,
                InputOption::VALUE_NONE,
                'If set and the chosen format supports it, the output will be a bit more human-readable'
            );
    }

    /**
     * Executes the command, printing out products according to the given parameters and options.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $uri = $input->getArgument('uri');

        $this->writelnAboveNormal($output, '<info>Getting product list...</info>');

        $productsRepository = $this->getProductsRepository();

        try {
            $products = $productsRepository->getAllInUri($uri);
        } catch (\Exception $exception) {
            $output->writeln('<error>Error hitting ' . $uri . '!</error>');
            $message = '<error>Error: ';
            $message .= $exception->getCode() . ': ' . $exception->getMessage() . '</error>';
            $this->writelnAboveNormal($output, $message);
            return 1;
        }

        $message = '<info>Requesting individual descriptions and direct product page sizes...</info>';
        $this->writelnAboveNormal($output, $message);
        $promises = $productsRepository->requestAllInfo($products);

        $responses = Promise\unwrap($promises);

        $total = 0;

        foreach ($responses as $index => $response) {
            $total += $products[$index]->getPrice();

            $message = '<info>Adding info for product "' . $products[$index]->getTitle() . '"</info>';
            $this->writelnAboveNormal($output, $message);

            try {
                $products[$index] = $productsRepository->expand($products[$index], $response);
            } catch (\Exception $exception) {
                $output->writeln('<comment>Error hitting ' . $products[$index]->getDirectUri() . '!</comment>');
                $message = '<comment>Error: ';
                $message .= $exception->getCode() . ': ' . $exception->getMessage() . '</comment>';
                $this->writelnAboveNormal($output, $message);
            }
        }

        $results = [
            'results' => $products,
            'total' => $total
        ];

        $format = $input->getOption('format');
        $pretty = $input->getOption('pretty');

        $message = '<info>Results ready to print as ' . $format . '!</info>';
        $this->writelnAboveNormal($output, $message);

        return $this->printResults($output, $results, $format, $pretty);
    }

    /**
     * @return ProductsRepository
     */
    protected function getProductsRepository()
    {
        return $this->productsRepository;
    }

    /**
     * Writes the given message in all modes above and including verbose.
     *
     * @param OutputInterface $output
     * @param string $message
     */
    protected function writelnAboveNormal(OutputInterface $output, $message)
    {
        if ($output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $output->writeln($message);
        }
    }

    /**
     * Prints out the results according to the specified format.
     *
     * @param OutputInterface $output
     * @param array $results
     * @param string $format
     * @param bool $pretty
     * @return int
     */
    protected function printResults(OutputInterface $output, array $results, $format = 'json', $pretty = false)
    {
        switch ($format) {
            case 'json':
                $options = $pretty ? JSON_PRETTY_PRINT : 0;
                $output->writeln(json_encode($results, $options));
                break;
            case 'array':
                $output->writeln(print_r($results));
                break;
            default:
                $output->writeln('<error>Error: Requested output format "' . $format . '" not supported!</error>');
                return 1;
        }

        return 0;
    }
}