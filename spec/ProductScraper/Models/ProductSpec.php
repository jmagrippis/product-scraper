<?php

namespace spec\ProductScraper\Models;


use PhpSpec\ObjectBehavior;
use ProductScraper\Models\Product;
use Prophecy\Argument;

class ProductSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Product::class);
        $this->shouldImplement(\JsonSerializable::class);
    }

    function it_returns_its_relevant_properties()
    {
        $title = 'Test Product X';
        $size = 200.1;
        $price = 0.42;
        $description = 'The tastiest test product.';
        $direct_uri = 'http://example.com/products/1';

        $this->beConstructedWith([
            'title' => $title,
            'size' => $size,
            'price' => $price,
            'description' => $description,
            'direct_uri' => $direct_uri
        ]);

        $properties = $this->jsonSerialize();
        $properties->shouldHaveCount(4);
        $properties->shouldHaveKeyWithValue('title', $title);
        $properties->shouldHaveKeyWithValue('size', $size / 1000 . 'kb');
        $properties->shouldHaveKeyWithValue('unit_price', $price);
        $properties->shouldHaveKeyWithValue('description', $description);

        $properties['unit_price']->shouldBeFloat();
    }

    function it_returns_null_for_relevant_properties_not_assigned()
    {
        $title = 'Test Product X';
        $price = 0.42;
        $direct_uri = 'http://example.com/products/1';

        $this->beConstructedWith([
            'title' => $title,
            'price' => $price,
            'direct_uri' => $direct_uri
        ]);

        $properties = $this->jsonSerialize();
        $properties->shouldHaveCount(4);
        $properties->shouldHaveKeyWithValue('title', $title);
        $properties->shouldHaveKeyWithValue('size', null);
        $properties->shouldHaveKeyWithValue('unit_price', $price);
        $properties->shouldHaveKeyWithValue('description', null);

        $properties['unit_price']->shouldBeFloat();
    }

    function it_only_assigns_properties_marked_as_fillable()
    {
        $title = 'Test Product X';
        $size = 200.1;

        $this->beConstructedWith([
            'title' => $title,
            'size' => $size,
            'prize' => 0.42,
            'hax' => 'DROP TABLE'
        ]);

        $properties = $this->jsonSerialize();
        $properties->shouldHaveCount(4);
        $properties->shouldHaveKeyWithValue('title', $title);
        $properties->shouldHaveKeyWithValue('size', $size / 1000 . 'kb');
        $properties->shouldHaveKeyWithValue('unit_price', null);
        $properties->shouldHaveKeyWithValue('description', null);

        $properties->shouldNotHaveKey('prize');
        $properties->shouldNotHaveKey('hax');
    }

    function it_adds_properties_to_a_clone_of_itself()
    {
        $title = 'Test Product X';
        $price = 0.42;

        $this->beConstructedWith([
            'title' => $title,
            'price' => $price
        ]);

        $properties = $this->jsonSerialize();
        $properties->shouldHaveCount(4);
        $properties->shouldHaveKeyWithValue('title', $title);
        $properties->shouldHaveKeyWithValue('size', null);
        $properties->shouldHaveKeyWithValue('unit_price', $price);
        $properties->shouldHaveKeyWithValue('description', null);

        $size = 200.1;
        $description = 'The tastiest test product.';

        $updated_product = $this->withAddedProperties([
            'size' => $size,
            'description' => $description
        ]);

        $updated_product->shouldHaveType(Product::class);

        $updated_properties = $updated_product->jsonSerialize();
        $updated_properties->shouldHaveCount(4);
        $updated_properties->shouldHaveKeyWithValue('title', $title);
        $updated_properties->shouldHaveKeyWithValue('size', $size / 1000 . 'kb');
        $updated_properties->shouldHaveKeyWithValue('unit_price', $price);
        $updated_properties->shouldHaveKeyWithValue('description', $description);
    }

    function it_adds_properties_without_overwriting_existing_ones()
    {
        $title = 'Test Product X';
        $price = 0.42;
        $direct_uri = 'http://example.com/products/1';

        $this->beConstructedWith([
            'title' => $title,
            'price' => $price,
            'direct_uri' => $direct_uri
        ]);

        $properties = $this->jsonSerialize();
        $properties->shouldHaveCount(4);
        $properties->shouldHaveKeyWithValue('title', $title);
        $properties->shouldHaveKeyWithValue('size', null);
        $properties->shouldHaveKeyWithValue('unit_price', $price);
        $properties->shouldHaveKeyWithValue('description', null);

        $size = 200.1;
        $description = 'The tastiest test product.';

        $updated_product = $this->withAddedProperties([
            'size' => $size,
            'description' => $description,
            'price' => 1.79,
            'title' => 'New Title'
        ]);

        $updated_product->shouldHaveType(Product::class);

        $updated_properties = $updated_product->jsonSerialize();
        $updated_properties->shouldHaveCount(4);
        $updated_properties->shouldHaveKeyWithValue('title', $title);
        $updated_properties->shouldHaveKeyWithValue('size', $size / 1000 . 'kb');
        $updated_properties->shouldHaveKeyWithValue('unit_price', $price);
        $updated_properties->shouldHaveKeyWithValue('description', $description);
    }

    function it_adds_only_fillable_properties()
    {
        $title = 'Test Product X';
        $price = 0.42;
        $direct_uri = 'http://example.com/products/1';

        $this->beConstructedWith([
            'title' => $title,
            'price' => $price,
            'direct_uri' => $direct_uri
        ]);

        $properties = $this->jsonSerialize();
        $properties->shouldHaveCount(4);
        $properties->shouldHaveKeyWithValue('title', $title);
        $properties->shouldHaveKeyWithValue('size', null);
        $properties->shouldHaveKeyWithValue('unit_price', $price);
        $properties->shouldHaveKeyWithValue('description', null);

        $size = 200.1;
        $description = 'The tastiest test product.';

        $updated_product = $this->withAddedProperties([
            'size' => $size,
            'description' => $description,
            'prize' => 0.42,
            'hax' => 'DROP TABLE'
        ]);

        $updated_product->shouldHaveType(Product::class);

        $updated_properties = $updated_product->jsonSerialize();
        $updated_properties->shouldHaveCount(4);
        $updated_properties->shouldHaveKeyWithValue('title', $title);
        $updated_properties->shouldHaveKeyWithValue('size', $size / 1000 . 'kb');
        $updated_properties->shouldHaveKeyWithValue('unit_price', $price);
        $updated_properties->shouldHaveKeyWithValue('description', $description);

        $properties->shouldNotHaveKey('prize');
        $properties->shouldNotHaveKey('hax');
    }

    function it_returns_its_direct_uri()
    {
        $title = 'Test Product X';
        $price = 0.42;
        $direct_uri = 'http://example.com/products/1';

        $this->beConstructedWith([
            'title' => $title,
            'price' => $price,
            'direct_uri' => $direct_uri
        ]);

        $this->getDirectUri()->shouldBe($direct_uri);
    }
}
