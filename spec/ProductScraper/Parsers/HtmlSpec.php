<?php

namespace spec\ProductScraper\Parsers;


use PhpSpec\ObjectBehavior;
use ProductScraper\Parsers\Html;
use ProductScraper\Parsers\ParserInterface;
use Prophecy\Argument;

class HtmlSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Html::class);
        $this->shouldImplement(ParserInterface::class);
    }

    function it_parses_all_products_from_an_html_multi_product_page()
    {
        $html = file_get_contents(__DIR__ . '/../../../resources/products/test_product_list.html');
        $this->beConstructedWith($html);
        $products = $this->getProducts();
        $products->shouldHaveCount(3);

        $products[0]->title->shouldBe('Sainsbury\'s Apricot Ripe & Ready x5');
        $products[0]->price->shouldBe(3.5);
        $products[0]->directUri->shouldBe('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-apricot-ripe---ready-320g.html');

        $products[1]->title->shouldBe('Sainsbury\'s Avocado Ripe & Ready XL Loose 300g');
        $products[1]->price->shouldBe(1.5);
        $products[1]->directUri->shouldBe('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-avocado-xl-pinkerton-loose-300g.html');

        $products[2]->title->shouldBe('Sainsbury\'s Avocado, Ripe & Ready x2');
        $products[2]->price->shouldBe(1.8);
        $products[2]->directUri->shouldBe('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-avocado--ripe---ready-x2.html');
    }

    function it_parses_the_description()
    {
        $description = '<h3 class="productDataItemHeader">Description</h3>
<div class="productText">
<p>Avocados</p>
<p>
<p></p>
</p>
</div>';
        $html = '<!DOCTYPE html><html>...' . $description . '...</html>';

        $this->beConstructedWith($html);

        $product = $this->getProduct();

        $product->description->shouldBe('Avocados');
        $product->size->shouldBe(150);
    }

    function it_allows_for_the_source_to_be_changed()
    {
        $description = '<h3 class="productDataItemHeader">Description</h3>
<div class="productText">
<p>/Everybody stand back/</p>
</div>';

        $html = '<!DOCTYPE html><html>...' . $description . '...</html>';

        $this->beConstructedWith($html);

        $product = $this->getProduct();

        $product->description->shouldBe('/Everybody stand back/');
        $product->size->shouldBe(147);

        $description = '<h3 class="productDataItemHeader">Description</h3>
<div class="productText">
<p>I know <strong>regular expressions</strong></p>
</div>';

        $html = '<!DOCTYPE html><html>...' . $description . '...</html>';

        $this->withSource($html);

        $newProduct = $this->withSource($html)->getProduct();

        $newProduct->description->shouldBe('I know regular expressions');
        $newProduct->size->shouldBe(168);
    }
}
