<?php

namespace spec\ProductScraper\Repositories;


use GuzzleHttp\ClientInterface;
use GuzzleHttp\Promise\PromiseInterface;
use PhpSpec\ObjectBehavior;
use ProductScraper\Exceptions\HttpResponse as HttpResponseException;
use ProductScraper\Models\Product;
use ProductScraper\Parsers\ParserInterface;
use ProductScraper\Repositories\ProductsHtml;
use ProductScraper\Repositories\ProductsRepositoryInterface;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;

class ProductsHtmlSpec extends ObjectBehavior
{
    function let(ClientInterface $client, ParserInterface $parser)
    {
        $this->beConstructedWith($client, $parser);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ProductsHtml::class);
        $this->shouldImplement(ProductsRepositoryInterface::class);
    }

    function it_returns_an_array_of_products_based_on_the_given_uri(
        $client,
        $parser,
        ResponseInterface $response,
        Product $product
    ) {
        $uri = 'http://example.com/latest-products';
        $responseCode = 200;
        $responseBody = '<!DOCTYPE html><html>...</html>';

        $response->getStatusCode()->willReturn($responseCode);
        $response->getBody()->willReturn($responseBody);
        $client->request('GET', $uri)->willReturn($response);

        $parser->withSource($responseBody)->willReturn($parser);
        $parser->getProducts()->willReturn([
            $product,
            $product,
            $product,
            $product,
            $product
        ]);

        $products = $this->getAllInUri($uri);
        $products->shouldHaveCount(5);
        $products[0]->shouldHaveType(Product::class);
    }

    function it_throws_an_exception_on_a_bad_response_when_getting_all_products(
        $client,
        ResponseInterface $response
    ) {
        $uri = 'http://example.com/invalid-endpoint';
        $responseCode = 404;
        $reasonPhrase = 'Not Found';

        $response->getStatusCode()->willReturn($responseCode);
        $response->getReasonPhrase()->willReturn($reasonPhrase);
        $client->request('GET', $uri)->willReturn($response);

        $this->shouldThrow(new HttpResponseException($reasonPhrase, $responseCode))->duringGetAllInUri($uri);
    }

    function it_requests_information_for_all_given_products_and_returns_promises(
        $client,
        Product $product,
        PromiseInterface $promise
    ) {
        $uri = 'http://example.com/product';
        $product->getDirectUri()->willReturn($uri);

        $client->requestAsync('GET', $uri)->willReturn($promise);

        $products = [
            $product,
            $product,
            $product,
            $product,
            $product
        ];

        $promises = $this->requestAllInfo($products);
        $promises->shouldHaveCount(5);
        $promises[0]->shouldHaveType(PromiseInterface::class);
    }

    function it_expands_a_given_product_by_parsing_the_given_request(
        $parser,
        Product $product,
        ResponseInterface $response
    ) {
        $responseCode = 200;
        $responseBody = '<!DOCTYPE html><html>...</html>';

        $response->getStatusCode()->willReturn($responseCode);
        $response->getBody()->willReturn($responseBody);

        $parser->withSource($responseBody)->willReturn($parser);
        $parser->getProduct()->willReturn($product);

        $size = 200.1;
        $description = 'The tastiest test product.';
        $additional_properties = [
            'size' => $size,
            'description' => $description
        ];

        $product->getProperties()->willReturn($additional_properties);
        $product->withAddedProperties($additional_properties)->willReturn($product);

        $expanded_product = $this->expand($product, $response);
        $expanded_product->shouldHaveType(Product::class);
    }

    function it_throws_an_exception_on_a_bad_response_when_expanding_a_products(
        $client,
        Product $product,
        ResponseInterface $response
    ) {
        $uri = 'http://example.com/invalid-endpoint';
        $responseCode = 404;
        $reasonPhrase = 'Not Found';

        $response->getStatusCode()->willReturn($responseCode);
        $response->getReasonPhrase()->willReturn($reasonPhrase);
        $client->request('GET', $uri)->willReturn($response);

        $this->shouldThrow(new HttpResponseException($reasonPhrase, $responseCode))->duringExpand($product, $response);
    }
}
