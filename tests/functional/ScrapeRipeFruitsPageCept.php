<?php 
$I = new FunctionalTester($scenario);
$I->wantTo('scrape the Ripe Fruits page and get a json array of all products');

$I->runShellCommand('php ./bin/scraper.php parse:uri');

$I->seeInShellOutput('{"results":[{"title":"Sainsbury\'s Apricot Ripe & Ready x5","size":"39.185kb"');
$I->seeInShellOutput('"size":"39.485kb","unit_price":1.8,"description":"Gold Kiwi"}');
$I->seeInShellOutput('"total":15.1}');