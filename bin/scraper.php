#!/usr/bin/env php
<?php

require __DIR__ . '/../vendor/autoload.php';

use GuzzleHttp\Client;
use ProductScraper\Console\Commands\ParseUri;
use ProductScraper\Parsers\Html as HtmlParser;
use ProductScraper\Repositories\ProductsHtml;
use Symfony\Component\Console\Application;

$application = new Application('product-scraper', '0.1');

$application->add(new ParseUri(new ProductsHtml(new Client(), new HtmlParser())));

$application->run();